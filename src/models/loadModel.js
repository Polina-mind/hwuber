const mongoose = require("mongoose");
const { LoadStatusesEnum } = require("../enums/loadsEnum");

const Load = mongoose.model("Load", {
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },

  name: String,

  pickup_address: String,

  delivery_address: String,

  created_by: mongoose.Schema.Types.ObjectId,

  logs: [],

  assigned_to: mongoose.Schema.Types.ObjectId,

  status: {
    type: LoadStatusesEnum,
    default: "NEW",
    required: true,
  },

  state: {
    type: String,
    default: "En route to Pick Up",
  },

  dimensions: Object,

  payload: Number,

  createdAt: {
    type: Date,
    default: Date.now(),
  },

  __v: { type: Number, select: false },
});

module.exports = { Load };
