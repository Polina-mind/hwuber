const { InvalidRequestError } = require("./errors");
const { getUserProfileById } = require("../services/userService");

const roleCheck = async (userId, role) => {
  const user = await getUserProfileById(userId);

  if (user.role !== role) {
    // console.log(result.error);
    throw new InvalidRequestError(`User role must be ${role}`);
  }
};

// const driverValidation = async (userId) => {
//   const user = await getUserProfileById(userId);

//   const result = driverRoleSchema.validate(user.role);
//   if (result.error) {
//     // console.log(result.error);
//     throw new InvalidRequestError(result.error);
//   }
// };

// const shipperValidation = async (userId) => {
//   const user = await getUserProfileById(userId);

//   const result = shipperRoleSchema.validate(user.role);
//   if (result.error) {
//     // console.log(result.error);
//     throw new InvalidRequestError(result.error);
//   }
// };

module.exports = { roleCheck };
