const bcrypt = require("bcrypt");

const { User } = require("../models/userModel");

const getUserProfileById = async (userId) => {
  const user = await User.findOne({ _id: userId }).select("-password");
  return user;
};

const deleteUserProfileById = async (userId) => {
  const user = await User.findOneAndDelete({ _id: userId });
  return user;
};

const changeUsersPasswordById = async (userId, oldPassword, newPassword) => {
  const user = await getUserProfileById(userId);

  if (!user) {
    throw new InvalidRequestError("No user with such id found");
  }

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new Error("Wrong oldPassword");
  }

  await User.updateOne(
    { _id: userId },
    {
      $set: {
        password: await bcrypt.hash(newPassword, 10),
      },
    }
  );
  return user;
};

module.exports = {
  getUserProfileById,
  deleteUserProfileById,
  changeUsersPasswordById,
};
