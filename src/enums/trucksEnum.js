const TruckTypesEnum = {
  SPRINTER: "SPRINTER",
  SMALL_STRAIGHT: "SMALL STRAIGHT",
  LARGE_STRAIGHT: "LARGE STRAIGHT",
};
Object.freeze(TruckTypesEnum);

const TruckStatusesEnum = {
  IS: "IS",
  OL: "OL",
};
Object.freeze(TruckStatusesEnum);

module.exports = { TruckTypesEnum, TruckStatusesEnum };
