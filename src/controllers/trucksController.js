const express = require("express");
const router = express.Router();

const {
  getTrucksByUserId,
  addTruckToUser,
  getTruckByIdForUser,
  deleteTruckByIdForUser,
  updateTruckByIdForUser,
  assignTruckByIdForUser,
  getAllTrucksByUserId,
} = require("../services/trucksService");

const { roleCheck } = require("../utils/roleCheck");
const { RolesEnum } = require("../enums/rolesEnum");
const { TruckTypesEnum, TruckStatusesEnum } = require("../enums/trucksEnum");

const { asyncWrapper } = require("../utils/apiUtils");
const { InvalidRequestError } = require("../utils/errors");

//getTrucksByUserId
router.get(
  "/",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { offset, limit } = req.query;

    await roleCheck(userId, RolesEnum.DRIVER);

    const trucks = await getTrucksByUserId(userId, offset, limit);

    res.json({ trucks });
  })
);

//addTruckToUser
router.post(
  "/",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    await roleCheck(userId, RolesEnum.DRIVER);

    await addTruckToUser(userId, req.body);
    res.json({ message: "Truck created successfully" });
  })
);

//getTrucksByIdForUser
router.get(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    await roleCheck(userId, RolesEnum.DRIVER);

    const truck = await getTruckByIdForUser(id, userId);

    if (!truck) {
      throw new InvalidRequestError("No truck with such id found!");
    }

    res.json({ truck });
  })
);

//updateTruckByIdForUser
router.put(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const data = req.body;

    await roleCheck(userId, RolesEnum.DRIVER);

    const truck = await getTruckByIdForUser(id, userId);

    if (!truck) {
      throw new InvalidRequestError("No truck with such id found!");
    }

    if (userId.includes(truck.assigned_to)) {
      throw new InvalidRequestError(
        "This truck assigned and cannot be changed!"
      );
    }

    await updateTruckByIdForUser(id, userId, data);

    res.json({ message: "Truck updated successfully" });
  })
);

//deleteTruckByIdForUser
router.delete(
  "/:id",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    await roleCheck(userId, RolesEnum.DRIVER);

    const truck = await getTruckByIdForUser(id, userId);

    if (!truck) {
      throw new InvalidRequestError("No truck with such id found!");
    }

    if (userId.includes(truck.assigned_to)) {
      throw new InvalidRequestError(
        "This truck assigned and cannot be deleted!"
      );
    }

    await deleteTruckByIdForUser(id, userId);
    res.json({ message: "Truck was deleted successful" });
  })
);

module.exports = {
  trucksRouter: router,
};

// assignTruckByIdForUser
router.post(
  ":id/assign",
  asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    await roleCheck(userId, RolesEnum.DRIVER);

    const truck = await getTruckByIdForUser(id, userId);
    if (!truck) {
      throw new InvalidRequestError("User is not owner of the truck!");
    }

    const trucks = getAllTrucksByUserId(userId);

    trucks.map((truck) => {
      if (
        truck.assigned_to === userId &&
        truck.status === TruckStatusesEnum.OL
      ) {
        throw new InvalidRequestError("Driver on load already!");
      }
    });

    await assignTruckByIdForUser(id, userId);

    res.json({ message: "Truck assigned successfully" });
  })
);

module.exports = {
  trucksRouter: router,
};
